subscription_id     = "${SUBSCRIPTION_ID}"
client_id           = "${CLIENT_ID}"
client_secret       = "${CLIENT_SECRET}"
tenant_id           = "${TENANT_ID}"
location            = "${INFRASTR_LOCATION}"
resource_group_name = "${RSCE_GROUP_NAME}"
acrname             = "${ACR_NAME}"
storage_account_name  = "${STORAGE_ACCOUNTNAME}"
storage_container_name = "${ACR_STORAGE_CONTAINER_NAME}"
access_key           = "${ACCESS_KEY}"