resource "azurerm_container_registry" "gorilla_example" {
    name = "${var.acrname}"
    resource_group_name = "${var.resource_group_name}"
    location = "${var.location}"
    sku = "Standard"
}
