terraform {
  backend "azurerm" {
    storage_account_name = "${STORAGE_ACCOUNTNAME}"
    container_name       = "${AKS_STORAGE_CONTAINER_NAME}"
    key                  = "terraform.tfstate"
    access_key           = "${ACCESS_KEY}"
  }
}
